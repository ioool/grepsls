
![grepSLS](grepSLS.png)

# grepSLS

This repository holds all the information about the grepSLS. grepSLS is an SLS machine designed and builded by the GREP research group from the University of Girona.

Our mission at the beggining of this project was to democratize the SLS technology by designing a machine with normalized elements and some 3D printed parts.

As Open Source is not all about code, all the CAD and laser cut files are also given. 


# How is it organized?

**BOM**

Lists of elements, components, quantity and materials with all the providers used to build the machine. If you use different components must be the same size and similar or higher specs.

**Mechanics**

All the CAD and mechanics files can be found here. CAD (SolidWorks files), LaserCut (DWG files) and STL (3D priting files). An image folder (img) and a 3DPDF of the assembly is provided to help you understand the machine and how you can mount it.

**Electronics**

Electrical schemes and how to connect the components.

**Configurations**

Software files to configure the Marlin (you can download the marlin [here](https://marlinfw.org/) and then you must replace the configuration.h file), grepsls_config.ini to import to [Slic3r](https://slic3r.org/) and posadaAPunt.gcode ("get ready" file, to avoid having to set up the machine manually).

**Render**

Render images to help understanding the machine and its components.


# Concept

The machine is completely open source and can be modified in order to satisfy your own necessities. It has been developed using the essence of an FDM 3D printer.

It has been designed to be super easy to build and replicate using affordable components and the available open source technology. It can be easily transformed to a laser cutter or an FDM printer (files and instructions coming soon).

**We have to start designing globally and producing locally**


![grepSLS](ca/render/grepSLS.png)



-------

This project was developed by GREP, Universitat de Girona (Catalonia)

![grep](GREP.jpg) ![eps](EPS.png)